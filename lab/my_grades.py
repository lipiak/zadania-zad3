# -*- encoding: utf-8 -*-

import getpass
import requests
from bs4 import BeautifulSoup
from xml.dom import minidom

url = {
    'login': 'https://canvas.instructure.com/login',
    'grades': 'https://canvas.instructure.com/courses/838324/grades'
}

username = raw_input('Użytkownik: ')
password = getpass.getpass('Hasło: ')

payload = {
    'pseudonym_session[unique_id]': username,
    'pseudonym_session[password]': password
}

# Logowanie
session = requests.session()
session.post(url['login'], data=payload)
# Pobranie strony z ocenami
response = session.get(url['grades'])
#response.status_code
#response.headers['content-type']
# Odczyt strony
html = response.text
# Parsowanie strony
parsed = BeautifulSoup(html, 'html5lib')
# Scraping
kategorie = []
linki = []
nazwy = []
oceny = []

for k in parsed.find_all('div', 'context'):
    if k not in kategorie:
        kategorie.append(k)


wszystkie_wiersze = parsed.find_all('tr', 'student_assignment  editable')
wszystkie_wiersze += parsed.find_all('tr', 'student_assignment assignment_graded editable')


for wiersz in wszystkie_wiersze:
    xml = minidom.parseString(str(wiersz))
    a = xml.getElementsByTagName('a')
    linki += a[0].attributes['href'].value
    nazwy += a[0].childNodes[0].toxml()
    o = xml.getElementsByTagName('span')



#wszystkie_tytuly = parsed.find_all('th', class_='title')
#print wszystkie_tytuly

# Sortowanie

# Wyświetlenie posortowanych ocen w kategoriach