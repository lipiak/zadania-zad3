# -*- encoding: utf-8 -*-

import requests
from bs4 import BeautifulSoup
from xml.dom import minidom
from texttable import Texttable

#pobieranie strony
session = requests.session()
response = session.get('http://tv.wp.pl/mindex.html')
html = response.text
parsed = BeautifulSoup(html, 'html5lib')

listaKanalow = []
listaTytulow = []
listaOcen = []


wyniki_pierwsza_czesc = parsed.find('section', {"id": "contentTop"})
wyniki_druga_czesc = parsed.find('section', {"id": "contentBottom"})

kanaly = wyniki_pierwsza_czesc.find_all('span', 'name') + wyniki_druga_czesc.find_all('span', 'name')

print u'Pobieram listę kanałów...'
for kanal in kanaly:
    if kanal.text not in listaKanalow:
        listaKanalow.append(kanal.text)

tytuly = wyniki_pierwsza_czesc.find_all('td', 'box box1') + wyniki_druga_czesc.find_all('td', 'box box1')

print u'Pobieram aktualnie trwające filmy/seriale/programy...'
for tytul in tytuly:
    title = (tytul.find('span', 'title')).text
    listaTytulow.append(title)

print u'Pobieram dostępne oceny do programów/seriali/filmów...\n\n\n'
for tyt in listaTytulow:
    query = 'http://www.filmweb.pl/search?q={0}'.format(str(tyt.encode('utf-8')).replace(' ', '+'))
    response = session.get(query)
    film_html = BeautifulSoup(response.text, 'html5lib')

    #sprawdzic tutaj, czy faktycznie analizujemy poszukiwany przez nas tytul
    filmweb_tytul = (film_html.find('a', 'hdr hdr-medium hitTitle'))
    if filmweb_tytul:




        box = film_html.find('div', 'box box-half')
        ocena = box.find('strong')
        listaOcen.append(ocena.text)

    else:
        listaOcen.append('brak')

#tworzenie tabelki, co by ładnie wypisać
table = Texttable()
table.set_deco(table.HEADER | table.VLINES)
table.set_cols_align(["l", "c", "c"])
table.header([u'Program', u'Tytul', u'Ocena'])

for i in range(0, len(listaKanalow)-1):
    table.add_row([u'{0}'.format(listaKanalow[i]).encode('utf-8'), u'{0}'.format(listaTytulow[i]).encode('utf-8'), u'{0}'.format(listaOcen[i])])
    #print u'{0}:\t\t{1}   | Ocena: {2}'.format(listaKanalow[i],listaTytulow[i],listaOcen[i])
print table.draw()