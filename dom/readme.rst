
Praca domowa #3
===============

Stworzyć bardziej złożony mashup dla tych danych, co Państwa interesują.
Przykłady:

* mapa rozmieszczenia browarów lub pubów w pobliżu domu
* wielo-osiowy wykres popularności różnych miast w kilku kategoriach
* wizualizacja głosowań danego polityka lub partii (np. http://mamprawowiedziec.pl/strona/glosowania/kategorie/7)

Masz zainteresowania, Sieć ma narzędzia — połącz je, by stworzyć coś nowego.
Możesz użyć `katalogu API <http://www.programmableweb.com/apis/directory>`__, żeby zobaczyć różnorodność
źródeł danych i wpaść na jakiś pomysł.

W pliku ABOUT napisz jakich źródeł danych używasz, z jakich narzędzi korzystasz oraz
jaką wartość dodaną (nową funkcjonalność z połączonych API) chciałeś stworzyć.

Stwórz testy sprawdzające poprawność zaimplementowanych funkcjonalności.